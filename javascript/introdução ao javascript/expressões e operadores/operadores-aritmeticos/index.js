
// Operadores aritmeticos
//   - multiplicação
console.log(5 * 1);
//   - divisão
console.log(12 / 2);
//   - soma
console.log(5 + 4);
//   - subtração
console.log(4 - 2);

//   - resto da divisão
console.log(4 % 2);
//   - incremento
let num = 0
console.log(++num);
//   - decremento
console.log(--num);
//   - exponencial
console.log(3 ** 3);


