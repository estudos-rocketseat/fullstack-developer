import * as el from './elements.js'
import * as timer from './timer.js'
import state from './state.js';

export function actionPlay() {
  state.isRunning = true
  timer.countDown()
}

export function actionStop() {
  state.isRunning = false
}

export function actionAddMinutes() {
  let minute = Number(el.minutes.textContent)
  if (minute >= 60) {
    return
  }
  minute = minute + 5
  state.minutes = minute
  timer.updateDisplay(minute, state.seconds)
}

export function actionRemoveMinutes() {
  let minute = Number(el.minutes.textContent)
  if (minute <= 0) {
    return
  }
  minute = minute - 5
  state.minutes = minute
  timer.updateDisplay(minute, 0)
}

export function soundTree() {
  console.log('tocando som da arvore');
}

export function soundCloundRain() {
  console.log('tocando som de chuva');
}

export function soundCoffe() {
  console.log('tocando som de cafe');
}

export function soundFire() {
  console.log('som de fogo');
}