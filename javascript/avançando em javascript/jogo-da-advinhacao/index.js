const screennOne = document.querySelector('.screen1')
const screennTwo = document.querySelector('.screen2')
const btnTry = document.getElementById('btnTry')
const btnReset = document.getElementById('btnReset')
let randomNumber = Math.round(Math.random() * 10)
let xAttempts = 1

btnTry.addEventListener('click', handleTryClick)
btnReset.addEventListener('click', handleResetClick)

function handleTryClick(event) {
  event.preventDefault();
  const inputNumber = document.querySelector('#inputNumber')

  if (Number(inputNumber.value) === randomNumber) {
    toggleScreen()

    screennTwo.querySelector('h2').innerText = `Acertou em ${xAttempts} tentativas`;
  } else {
    alert('Tente novamente')
  }
  inputNumber.value = ""
  xAttempts++
}

function handleResetClick() {
  toggleScreen()
  randomNumber = Math.round(Math.random() * 10)
  xAttempts = 1
}

function toggleScreen() {
  screennOne.classList.toggle('hide')
  screennTwo.classList.toggle('hide')
}