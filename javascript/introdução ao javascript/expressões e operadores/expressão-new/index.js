/*
  new -> cria uma instancia de um objeto
*/

let name = new String("Emanuel")
name.surName = "Barros"
let age = new Number(22)

console.log(name, age)