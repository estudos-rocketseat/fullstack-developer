import { controls, sounds } from "./elements.js";
import * as actions from './actions.js'

export function registerControls() {
  controls.addEventListener('click', (event) => {
    const action = event.target.dataset.action
    if (actions[action]() != 'function') {
      return;
    }
    console.log(actions[action]());
  })
}

export function registerControlsSounds() {
  sounds.addEventListener('click', (e) => {
    const actionSound = e.target.dataset.action
    if (actions[actionSound]() != 'function') {
      return;
    }
    console.log(actions[actionSound]());
  })
}