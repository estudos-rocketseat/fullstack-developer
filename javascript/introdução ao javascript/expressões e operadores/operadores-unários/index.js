/*
  operadores unários
  typeof
  delete
*/

console.log(typeof "emanuel");

const person = {
  name: "Emanuel",
  age: 22
}

console.log(person);
delete person.age // deleta a propriedade age
console.log(person);