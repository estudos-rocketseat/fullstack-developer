import state from "./state.js"
import * as el from './elements.js'
import { actionStop } from "./actions.js"

export function updateDisplay(minutes, seconds) {
  minutes = minutes ?? state.minutes   //nullish coalesing operator
  seconds = seconds ?? state.seconds

  el.minutes.textContent = String(minutes).padStart(2, "0")
  el.seconds.textContent = String(seconds).padStart(2, "0")
}

export function countDown() {
  clearTimeout(state.countDownId)

  if (!state.isRunning) {
    return
  }

  let minutes = Number(el.minutes.textContent)
  let seconds = Number(el.seconds.textContent)

  seconds--

  if (seconds < 0) {
    seconds = 59
    minutes--
  }

  if (minutes < 0) {
    actionStop()
    return
  }

  state.countDownId = setTimeout(() => countDown(), 1000)
  console.log(state.countDownId);
  updateDisplay(minutes, seconds)
}