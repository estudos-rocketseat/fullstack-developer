// Operadores lógicos

// - 2 valores booleanos, quando verificados, resultará em verdadeiro ou 

let pao = false
let queijo = true

// AND &&
console.log(pao && queijo);

// OR ||
console.log(pao || queijo);

// NOT !
console.log(!pao);